<?php
$data = explode('.', $_SERVER['HTTP_HOST']);
if(isset($data[3])){
    $link = 'http://'.$data[1].'.'.$data[2].'.'.$data[3];
}else{
    $link = 'http://'.$data[1].'.'.$data[2];
}

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'news.upload.path' => realpath(__DIR__ . '/../../frontend/web/uploads/news'),
    'news.path' => $link.'/uploads/news',
    'category.upload.path' => realpath(__DIR__ . '/../../frontend/web/uploads/category'),
    'category.path' => $link.'/uploads/category',
];
