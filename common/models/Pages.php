<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $title
 * @property string $text
 * @property integer $status
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['text'], 'string'],
            [['status'], 'integer'],
            [['meta_keywords', 'meta_description', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'title' => 'Название',
            'text' => 'Текст',
            'status' => 'Активен/Не активен',
        ];
    }
}
