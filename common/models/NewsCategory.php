<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_category".
 *
 * @property integer $id
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $title
 * @property string $description
 * @property integer $status
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['meta_keywords', 'meta_description', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'title' => 'Название',
            'description' => 'Описание',
            'status' => 'Активен/Не активен',
        ];
    }
}
