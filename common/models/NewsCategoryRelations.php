<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_category_relations".
 *
 * @property integer $id
 * @property integer $news_id
 * @property integer $category_id
 */
class NewsCategoryRelations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_category_relations}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'category_id'], 'required'],
            [['news_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News ID',
            'category_id' => 'Category ID',
        ];
    }
}
