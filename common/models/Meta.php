<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "meta".
 *
 * @property integer $id
 * @property string $name
 * @property string $key
 * @property string $value
 */
class Meta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%meta}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'key'], 'required'],
            [['value'], 'string'],
            [['name', 'key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'key' => 'Meta Key',
            'value' => 'Meta Value',
        ];
    }

    /**
     * @param $key
     * @return mixed|string
     * get Meta by key
     */
    public function meta_by_key($key){
        return Meta::find()->where(['key' => $key])->one();
    }

    /**
     * @param $key
     * @return mixed|string
     * get Value by key
     */
    public function meta_value_by_key($key){
        $meta = Meta::find()->where(['key' => $key])->one();
        return $meta ? $meta->value : '';
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }
    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
