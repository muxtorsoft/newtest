<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\NewsCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'description')->widget(\vova07\imperavi\Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'paragraphize' => false,
            'cleanOnPaste' => false,
            'replaceDivs' => false,
            'linebreaks' => false,
            'allowedTags' => ['p','div','a','img', 'iframe', 'b', 'br', 'ul', 'li',
                'details', 'summary', 'span', 'h1', 'h2', 'h3', 'label'],
            'plugins' => [
                'fullscreen',
                'imagemanager',
//                'video'
            ],
            'imageUpload' => Url::to(['/news/image-upload']),
            'imageManagerJson' => Url::to(['/news/images-get']),
        ]
    ])->hint('Для втавки html-кода необходимо включить режим "Код". Разрешенные теги "p", "div", "a", "img", "iframe", "b", "br", "ul", "li",
                "details", "summary", "span", "h1", "h2", "h3", "label"');

    ?>
    <?//= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->radioList(common\models\News::statusLabels()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
