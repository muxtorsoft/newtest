<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать новости', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'meta_keywords',
            'meta_description',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($model) {
                    if(!empty($model->image)){
                        return Html::img(\Yii::$app->params['news.path'].'/'.$model->image);
                    }
                }
            ],
            [
                'attribute' => 'status', 
                'value' => function ($model) { 
                    return \common\models\News::statusLabels()[$model->status];
                }
            ],

            // 'anons:ntext',
            // 'text:ntext',
            // 'publish_date',
            // 'status',
            // 'created',
            // 'updated',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
