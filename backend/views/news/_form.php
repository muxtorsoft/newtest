<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal, clearfix'
        ]
    ]); ?>

    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

    <?php if(!empty($model->image)){
        echo Html::img(\Yii::$app->params['news.path'].'/'.$model->image);
    }?>
    <?= $form->field($model, 'image')->fileInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'anons')->textarea(['rows' => 6]) ?>

    <?=
    $form->field($model, 'text')->widget(\vova07\imperavi\Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'paragraphize' => false,
            'cleanOnPaste' => false,
            'replaceDivs' => false,
            'linebreaks' => false,
            'allowedTags' => ['p','div','a','img', 'iframe', 'b', 'br', 'ul', 'li',
                'details', 'summary', 'span', 'h1', 'h2', 'h3', 'label'],
            'plugins' => [
                'fullscreen',
                'imagemanager',
//                'video'
            ],
            'imageUpload' => Url::to(['/news/image-upload']),
            'imageManagerJson' => Url::to(['/news/images-get']),
        ]
    ])->hint('Для втавки html-кода необходимо включить режим "Код". Разрешенные теги "p", "div", "a", "img", "iframe", "b", "br", "ul", "li",
                "details", "summary", "span", "h1", "h2", "h3", "label"');

    ?>

    <?= $form->field($model, 'cats')->checkboxList(\yii\helpers\ArrayHelper::map(common\models\NewsCategory::find()->all(),'id','title')) ?>

    <?=$form->field($model, 'publish_date')->widget(kartik\datetime\DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Выберите дата публикации ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii:ss',
            //'startDate' => '01-Mar-2014 12:00 AM',
            'todayHighlight' => true
        ]
    ]);
    ?>

    <?= $form->field($model, 'status')->radioList(common\models\News::statusLabels()) ?>

    <?//= $form->field($model, 'created')->textInput() ?>

    <?//= $form->field($model, 'updated')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
