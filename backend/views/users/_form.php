<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?php if ($model->isNewRecord) { ?>
        <?= $form->field($model, 'pass')->textInput(['maxlength' => true]) ?>
    <?php } ?>
    <?//= $form->field($model, 'status')->dropDownList(common\models\Admins::statusLabels()) ?>

    <?= $form->field($model, 'role')->dropDownList(common\models\User::rolesLabels()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
