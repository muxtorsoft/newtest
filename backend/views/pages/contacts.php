<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 10.02.2017
 * Time: 10:50
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ContactsForm */
/* @var $form ActiveForm */
?>
<div class="pages">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'description')->textarea(['rows'=>6]) ?>
    <?= $form->field($model, 'phone') ?>
    <?= $form->field($model, 'address') ?>
    <?= $form->field($model, 'socail_fb') ?>
    <?= $form->field($model, 'socail_vk') ?>
    <?= $form->field($model, 'socail_ok') ?>
    <div>
        <label>Адрес для поиска: </label><br/><input id="address" style="width:600px;" name="test" type="text"/><br/><br/>
        <div id="ymap_container" style="width: 100%; height: 400px; display: block;" class="YMaps YMaps-cursor-grab"></div>
        <a href="#" id="ymap_container_show_map" class="form-link" style="">скрыть карту</a><br/>
        <?php echo $form->field($model, 'lat')->hiddenInput(['maxlength' => true])->label(false); ?>
        <?php echo $form->field($model, 'lng')->hiddenInput(['maxlength' => true])->label(false); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- pages -->
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://api-maps.yandex.ru/1.1/index.xml" type="text/javascript"></script>
<script type="text/javascript" src="/js/show_on_map.js"></script>
<script>
    $(document).ready(function(){
        initShowOnMap('ymap_container', 'ymap_container_show_map', 'contactsform-lat', 'contactsform-lng', true);
    });
</script>