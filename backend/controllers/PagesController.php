<?php

namespace backend\controllers;

use backend\models\ContactsForm;
use common\models\Meta;
use Yii;
use common\models\Pages;
use common\models\search\PagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Addational info for Contacts page.
     * @return mixed
     */
    public function actionContacts()
    {
        $model = new ContactsForm();
        //get meta data
        $desc = Meta::find()->where(['key' => 'page_contacts_description'])->one();
        $phone = Meta::find()->where(['key' => 'page_contacts_phone'])->one();
        $address = Meta::find()->where(['key' => 'page_contacts_address'])->one();
        $fb = Meta::find()->where(['key' => 'page_contacts_fb'])->one();
        $vk = Meta::find()->where(['key' => 'page_contacts_vk'])->one();
        $ok = Meta::find()->where(['key' => 'page_contacts_ok'])->one();
        $lat = Meta::find()->where(['key' => 'page_contacts_lat'])->one();
        $lng = Meta::find()->where(['key' => 'page_contacts_lng'])->one();
        //set meta data to model
        $model->description = $desc->value;
        $model->phone = $phone->value;
        $model->address = $address->value;
        $model->socail_fb = $fb->value;
        $model->socail_vk = $vk->value;
        $model->socail_ok = $ok->value;
        $model->lat = $lat->value;
        $model->lng = $lng->value;

        if ($model->load(Yii::$app->request->post())) {
            //set model data to meta data
            $desc->value = $model->description;
            $desc->update();
            $phone->value = $model->phone;
            $phone->update();
            $address->value = $model->address;
            $address->update();
            $fb->value = $model->socail_fb;
            $fb->update();
            $vk->value = $model->socail_vk;
            $vk->update();
            $ok->value = $model->socail_ok;
            $ok->update();
            $lat->value = $model->lat;
            $lat->update();
            $lng->value = $model->lng;
            $lng->update();

            \Yii::$app->getSession()->setFlash('success', 'Изменения успешно сохранены');
            return $this->redirect(['contacts']);
        }
        return $this->render('contacts', [
            'model' => $model,
        ]);
    }
}
