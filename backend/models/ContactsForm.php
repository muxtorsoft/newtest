<?php
namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class ContactsForm extends Model
{
    public $description;
    public $phone;
    public $address;
    public $socail_fb;
    public $socail_vk;
    public $socail_ok;
    public $lat;
    public $lng;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'phone', 'address', 'socail_fb', 'socail_vk', 'socail_ok'], 'required'],
            [['lat', 'lng'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => 'Описание',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'socail_fb' => 'Facebook',
            'socail_vk' => 'Вконтакте',
            'socail_ok' => 'Одноклассники',
            'lat' => 'Широта',
            'lng' => 'Долгота',
        ];
    }
}
