
	function initShowOnMap(container, expand_link, lat_field, lng_field, enable_geocoding, geocode_with_manual_coordinates, callback) {
		
		var mycontainer = '#' + container;
		var mylat_field = '#' + lat_field;
		var mylng_field = '#' + lng_field;
		var myexpand_link = '#' + expand_link;

		var default_lat = 55.753559;
		var default_lng = 37.609218;
		
		var coordinates_set_manually = 0;
		var autoexpand = 0;
		var default_zoom = 10;
		
		if (jQuery(mylat_field).val() != '' && jQuery(mylng_field).val() != '') {
			autoexpand = 1;
			default_zoom = 15;
		}
		
		if (jQuery(mylat_field).val() == '') jQuery(mylat_field).val(default_lat);
		if (jQuery(mylng_field).val() == '') jQuery(mylng_field).val(default_lng);
		
        YMaps.jQuery(function () {

            map = new YMaps.Map(mycontainer);

            map.setCenter(new YMaps.GeoPoint(jQuery(mylng_field).val(), jQuery(mylat_field).val()), default_zoom);            
            var zoom = new YMaps.Zoom();
            map.addControl(zoom);
            map.disableDblClickZoom();
            map.disableScrollZoom();
            
            
            var placemark = new YMaps.Placemark(map.getCenter(), {draggable: true});
            map.addOverlay(placemark);
            
            
            YMaps.Events.observe(placemark, placemark.Events.Drag, function (obj) {
                var current = obj.getGeoPoint().copy();
                jQuery(mylat_field).val(current.getLat());
                jQuery(mylng_field).val(current.getLng());
                coordinates_set_manually = 1;
            });
            
            YMaps.Events.observe(map, map.Events.DblClick, function(map, event) {                    
                var current = event.getGeoPoint();
                placemark.setGeoPoint(current);
                jQuery(mylat_field).val(current.getLat());
                jQuery(mylng_field).val(current.getLng());
                coordinates_set_manually = 1;
            });


            $(myexpand_link).show().click(function(){
                if ($(mycontainer).css('display') == 'none') {
			
                	$(mycontainer).show(100);
                	$(myexpand_link).html('скрыть карту');  
            		if (jQuery(mylat_field).val() == '' || jQuery(mylng_field).val() == '') {
            			jQuery(mylat_field).val(default_lat);
            			jQuery(mylng_field).val(default_lng);            			
            		}
                	map.setCenter(new YMaps.GeoPoint(jQuery(mylng_field).val(), jQuery(mylat_field).val()), default_zoom);
                    var current = placemark.getGeoPoint();                        
                    $(mylng_field).val(current.getLng());
                    $(mylat_field).val(current.getLat());

                }
                else {
                	$(mycontainer).hide(100);
                	$(myexpand_link).html('показать карту');
                	default_lat = jQuery(mylat_field).val();
                	default_lng = jQuery(mylng_field).val();
                	default_zoom = map.getZoom();
                	$(mylat_field + ', ' + mylng_field).val('');                        
                }
                
                return false;
            });
            
            if(autoexpand) $(myexpand_link).click();
            
            enable_geocoding = typeof(enable_geocoding) != 'undefined' ? enable_geocoding : false;
            
            geocode_with_manual_coordinates = typeof(geocode_with_manual_coordinates) != 'undefined' ? geocode_with_manual_coordinates : false;
            
            
            if (enable_geocoding) {
            	
            	function geocode_address() {
            		if (coordinates_set_manually && !geocode_with_manual_coordinates) return;
            		var address = $('#address').val();
            		
            		var geocoder = new YMaps.Geocoder(address);
            		YMaps.Events.observe(geocoder, geocoder.Events.Load, function () {
            		    if (this.length()) {            		        
            		        var first_result = this.get(0).getGeoPoint(); 
            		        var lat = first_result.getLat();
            		        var lng = first_result.getLng();
                			jQuery(mylat_field).val(lat);
                			jQuery(mylng_field).val(lng);
                			//if ($(mycontainer).css('display') != 'none') {
                				placemark.setGeoPoint(first_result);
                				map.setCenter(first_result);
                				switch(this.get(0).kind) {
            						case 'country': 
            							map.setZoom(8);
            							break;

                					case 'locality': 
                						map.setZoom(10);
                						break;
                						
                					case 'street':
                						map.setZoom(13);
                						break;

                					case 'house':
                						map.setZoom(15);
                						break;

                				}
                			//}                			
            		    }
            		})
            		
            		//alert(address);
            	}

                $(function(){
                    if (callback) {
                        callback(geocode_address)
                    }
                });
            	
            	$('#address').change(function(){
            		geocode_address();
            	});
				$('#address').keyup(function(){
					geocode_address();
				});

            }


        });

	}