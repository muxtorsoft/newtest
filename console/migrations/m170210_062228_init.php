<?php

use yii\db\Migration;

class m170210_062228_init extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%meta}} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
 
REPLACE INTO {{%meta}} (`id`, `name`, `key`, `value`) VALUES
	(1, 'Страницы контакты - Описание', 'page_contacts_description', NULL),
	(2, 'Телефон', 'page_contacts_phone', NULL),
	(3, 'Адрес', 'page_contacts_address', NULL),
	(4, 'Фейсбук', 'page_contacts_fb', NULL),
	(5, 'Вконтакте', 'page_contacts_vk', NULL),
	(6, 'Одноклассники', 'page_contacts_ok', NULL),
	(7, 'Широта', 'page_contacts_lat', NULL),
	(8, 'Долгота', 'page_contacts_lng', NULL); 

 
CREATE TABLE IF NOT EXISTS {{%news}} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `anons` text NOT NULL,
  `text` text NOT NULL,
  `publish_date` datetime NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
 
 
CREATE TABLE IF NOT EXISTS {{%news_category}} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
 
REPLACE INTO {{%news_category}} (`id`, `meta_keywords`, `meta_description`, `title`, `description`, `status`) VALUES
	(1, 'Тест', 'Тест', 'Тест', 'Тест Тест Тест', 1); 

 
CREATE TABLE IF NOT EXISTS {{%news_category_relations}} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8; 
 
CREATE TABLE IF NOT EXISTS {{%pages}} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS {{%user}} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 
REPLACE INTO {{%user}} (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `role`) VALUES
	(1, 'admin', '89l9Wz35DkaUcE9mCQ2aYRwElFCl6JAG', '$2y$13$.PVg7yYi7eaQV0paBkmC0OjaU4JafoCPmaotC73s83g5Ud9LcGBW6', NULL, 'admin@mail.ru', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'admin');
 
");
    }

    public function down()
    {
        echo "m170210_062228_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
