<?php
namespace frontend\controllers;

use common\models\News;
use common\models\NewsCategory;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * News controller
 */
class NewsController extends \yii\web\Controller
{
    public function actionCategory()
    {
        $dataProvider = new ActiveDataProvider([
            'query' =>NewsCategory::find()
                ->where(['status'=>News::STATUS_ACTIVE])
                ->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $this->view->title = 'Категории';
        return $this->render('category', ['listDataProvider' => $dataProvider]); 
    }

    /**
     * News items for category.
     *
     * @return mixed
     */
    public function actionCatView($id)
    {
        $cat = NewsCategory::findOne($id);
        if($cat==null)
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => News::find()
                ->leftJoin('tst_news_category_relations','tst_news.id=tst_news_category_relations.news_id')
                ->where('tst_news_category_relations.category_id=:id AND status=:status',[':id'=>(int)$id,':status'=>News::STATUS_ACTIVE])
                ->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $this->view->title = 'Статьи категорию: '.$cat->title;
        return $this->render('cat-view', ['listDataProvider' => $dataProvider]);
    }
    /**
     * News items.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find()
                ->where(['status'=>News::STATUS_ACTIVE])
                ->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $this->view->title = 'Статьи';
        return $this->render('index', ['listDataProvider' => $dataProvider]);
    }

    /**
     * News item view.
     *
     * @return mixed
     */
    public function actionView($id)
    {
        if (($model = News::find()->where('id=:id AND status=:status',[':id'=>(int)$id,':status'=>News::STATUS_ACTIVE])->one()) !== null)
        {
            return $this->render('view',
                ['model'=>$model]
            );
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
    }

}
