<?php

namespace frontend\controllers;

use common\models\News;
use common\models\Pages;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Page controller
 */
class PagesController extends \yii\web\Controller
{
    /**
     * News items.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Pages::find()
                ->where(['status'=>News::STATUS_ACTIVE])
                ->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $this->view->title = 'Страницы';
        return $this->render('index', ['listDataProvider' => $dataProvider]);
    }

    /**
     * News item view.
     *
     * @return mixed
     */
    public function actionView($id)
    {
        if (($model = Pages::find()->where('id=:id AND status=:status',[':id'=>(int)$id,':status'=>News::STATUS_ACTIVE])->one()) !== null)
        {
            return $this->render('view',
                ['model'=>$model]
            );
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

}
