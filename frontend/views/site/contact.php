<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <p>
                О нас: <?= nl2br($desc) ?>
            </p>
            <p>
                Телефон: <?= $phone ?>
            </p>
            <p>
                Адрес: <?= $address?>
            </p>
            <p>
                Социалный ссылки:
                <a href="<?= $fb?>">Фейсбук</a>
                <a href="<?= $ok?>">Одноклассники</a>
                <a href="<?= $vk?>">Вконтатке</a>
            </p>
            <div id="ymap_container" style="width: 100%; height: 400px; display: block;" class="YMaps YMaps-cursor-grab"></div>
        </div>
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7(999)999-9999',
                ]) ?>

                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>

<script>
    <?php
    $lat = empty($lat)?"37.609218":$lat;
    $lng = empty($lng)?"55.753559":$lng;
    $this->registerJsFile('http://api-maps.yandex.ru/1.1/index.xml');
    $this->registerJs("$(document).ready(function(){
    YMaps.jQuery(function () {
            // Создает экземпляр карты и привязывает его к созданному контейнеру
            var map = new YMaps.Map(YMaps.jQuery(\"#ymap_container\")[0]);
            
            // Устанавливает начальные параметры отображения карты: центр карты и коэффициент масштабирования
            map.setCenter(new YMaps.GeoPoint($lat,$lng), 10);
            var placemark = new YMaps.Placemark(new YMaps.GeoPoint($lat,$lng));

            // Устанавливает содержимое балуна
            placemark.name = \"$address\";

            // Добавляет метку на карту
            map.addOverlay(placemark);
        });        
        
    });");?>

</script>
