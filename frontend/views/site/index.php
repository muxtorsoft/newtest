<?php

/* @var $this yii\web\View */

$this->title = 'Проект для тестерование готов';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Готов!</h1>

        <p class="lead"><?=$desc ?></p>
    </div>

    <div class="body-content">
        <h2>Популярные новости</h2>
        <div class="row">
            <?php
            if($populNews){
                foreach ($populNews as $item){?>
                    <div class="col-lg-4">
                        <h2><?=$item->title?></h2>

                        <p><?=$item->anons?></p>

                        <p><?=\yii\helpers\Html::a('Читать дальше &raquo;',\yii\helpers\Url::to(['news/view','id'=>$item->id]),['class'=>'btn btn-default'])?></p>
                    </div>
                <?php }
            }
            ?>
        </div>

    </div>
</div>
