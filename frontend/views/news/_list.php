<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 10.02.2017
 * Time: 12:36
 * @var $model common\models\News
 */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>


<div class="news-item">
    <h2><?= Html::encode($model->title) ?></h2>
    <?= HtmlPurifier::process($model->anons) ?>
    <p><?=\yii\helpers\Html::a('Читать дальше &raquo;',\yii\helpers\Url::to(['news/view','id'=>$model->id]),['class'=>'btn btn-default'])?></p>
</div>