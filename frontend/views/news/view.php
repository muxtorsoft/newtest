<?php
/* @var $this yii\web\View */
$this->title = $model->title;
?>
<h1><?=$this->title?></h1>

<?php if(!empty($model->image))
{?>
   <div><img src="/uploads/news/<?=$model->image?>" alt=""></div> 
<?php } ?>
<p>
    <?=$model->text?>
</p>
