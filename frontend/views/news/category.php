<?php
/* @var $this yii\web\View */
use yii\widgets\ListView;
?>
    <h1><?=$this->title?></h1>

<?= ListView::widget([
    'dataProvider' => $listDataProvider,
    'itemView' => '_list_category',
    'emptyText' => 'Список пуст',
    'pager' => [
        'firstPageLabel' => 'Первая',
        'lastPageLabel' => 'Последняя',
        'nextPageLabel' => 'Следующая',
        'prevPageLabel' => 'Предыдущая',
        'maxButtonCount' => 5,
    ],
    'layout' => "{pager}\n{items}\n{pager}",//\n{summary}
    'summary' => 'Показано {count} из {totalCount}',
    'summaryOptions' => [
        'tag' => 'span',
        'class' => 'my-summary'
    ],
]);
